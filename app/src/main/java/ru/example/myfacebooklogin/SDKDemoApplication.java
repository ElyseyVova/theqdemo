package ru.example.myfacebooklogin;

import android.app.Application;
import live.stream.theq.theqkit.TheQConfig;
import live.stream.theq.theqkit.TheQKit;

public class SDKDemoApplication extends Application {

  @Override public void onCreate() {
    super.onCreate();

    TheQConfig theQConfig = new TheQConfig.Builder(this)
        .baseUrl("https://api.theq.live")
        .partnerCode("3df2ced2-44f6-46cd-9f5c-24e297b8eddd")
        .debuggable(BuildConfig.DEBUG)
        .build();

    TheQKit.getInstance()
        .init(theQConfig);
  }
}
