package ru.example.myfacebooklogin

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import live.stream.theq.theqkit.data.sdk.GameResponse
import live.stream.theq.theqkit.util.CurrencyHelper

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date

class GameRecyclerAdapter(private val context: Context, private val listener: GameClickedListener?) :
    RecyclerView.Adapter<GameRecyclerAdapter.GameViewHolder>() {

    private var games: List<GameResponse> = ArrayList()

    inner class GameViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        private val type: TextView
        private val payout: TextView
        private val scheduledTime: TextView
        private val playGame: Button
        private val dateFormat: DateFormat

        init {
            type = v.findViewById(R.id.gameType)
            payout = v.findViewById(R.id.gamePayout)
            scheduledTime = v.findViewById(R.id.scheduledTime)
            playGame = v.findViewById(R.id.playButton)
            dateFormat = SimpleDateFormat.getDateTimeInstance()
        }

        fun bind(game: GameResponse) {
            type.text = game.gameType
            payout.text = CurrencyHelper.getRoundedCurrency(context, game.reward)
            scheduledTime.text = dateFormat.format(Date(game.scheduled))

            if (game.active) {
                scheduledTime.visibility = View.GONE
                playGame.visibility = View.VISIBLE
                playGame.setOnClickListener {
                    listener?.onGameClicked(game)
                }
            } else {
                playGame.visibility = View.GONE
                scheduledTime.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_game, parent, false)

        return GameViewHolder(view)
    }

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        holder.bind(games[position])
    }

    override fun getItemCount(): Int {
        return games.size
    }

    fun setGames(games: List<GameResponse>) {
        this.games = games
        notifyDataSetChanged()
    }

    interface GameClickedListener {
        fun onGameClicked(game: GameResponse)
    }
}