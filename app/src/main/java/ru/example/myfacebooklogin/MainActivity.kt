package live.stream.theqkit.demo

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.facebook.accountkit.AccountKit
import live.stream.theq.theqkit.TheQKit
import live.stream.theq.theqkit.data.sdk.ApiError
import live.stream.theq.theqkit.data.sdk.GameResponse
import live.stream.theq.theqkit.listener.GameResponseListener
import ru.example.myfacebooklogin.GameRecyclerAdapter
import ru.example.myfacebooklogin.LoginActivity
import ru.example.myfacebooklogin.R

class MainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener, GameResponseListener,
    GameRecyclerAdapter.GameClickedListener {

    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var recyclerView: RecyclerView? = null
    private var cashoutButton: Button? = null
    private var logoutButton: Button? = null

    private var gameRecyclerAdapter: GameRecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_list)

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout)
        recyclerView = findViewById(R.id.recyclerView)
        cashoutButton = findViewById(R.id.cashoutButton)
        logoutButton = findViewById(R.id.logoutButton)

        swipeRefreshLayout!!.setOnRefreshListener(this)

        cashoutButton!!.setOnClickListener { TheQKit.getInstance().launchCashoutDialog(this@MainActivity) }

        logoutButton!!.setOnClickListener {
            TheQKit.getInstance().logout()
            AccountKit.logOut()
            val loginActivityIntent = Intent(this@MainActivity, LoginActivity::class.java)
            loginActivityIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(loginActivityIntent)
        }

        recyclerView!!.layoutManager = LinearLayoutManager(this)
        gameRecyclerAdapter = GameRecyclerAdapter(this, this)
        recyclerView!!.adapter = gameRecyclerAdapter

        loadGames()
    }

    private fun loadGames() {
        swipeRefreshLayout!!.isRefreshing = true
        TheQKit.getInstance().fetchGames(this)
    }

    override fun onGameClicked(game: GameResponse) {
        TheQKit.getInstance().launchGameActivity(this, game)
    }

    override fun onSuccess(games: List<GameResponse>) {
        swipeRefreshLayout!!.isRefreshing = false
        gameRecyclerAdapter!!.setGames(games)
    }

    override fun onFailure(apiError: ApiError) {
        swipeRefreshLayout!!.isRefreshing = false
    }

    override fun onRefresh() {
        loadGames()
    }
}
